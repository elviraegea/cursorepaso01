codeunit 50102 "TCN_Prueba"
{
    procedure ActualizarIdUnicoSalesLineF()
    var
        rlSalesLine: Record "Sales Line";
        xlGuidNulo: Guid;
    begin
        rlSalesLine.SetFilter(TCN_IdUnico, '<>%1', xlGuidNulo);
        if rlSalesLine.FindSet(true) then begin
            repeat
                rlSalesLine.AsignarIdUnicoF();
                rlSalesLine.Modify(true);
            until rlSalesLine.Next() = 0;
        end;
    end;
}