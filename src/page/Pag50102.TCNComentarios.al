page 50102 "TCN_Comentarios"
{

    Caption = 'TCN_Comentarios';
    PageType = List;
    SourceTable = TCN_ComentarioC_R;
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Comentario; Comentario)
                {
                    ApplicationArea = All;
                }
                field(IdUnicoOrigen; IdUnicoOrigen)
                {
                    ApplicationArea = All;
                }
                field(NumLinea; NumLinea)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
