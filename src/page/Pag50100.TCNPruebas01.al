page 50100 "TCN_Pruebas01"
{

    ApplicationArea = All;
    Caption = 'TCN_Pruebas01';
    PageType = List;
    SourceTable = TCN_Prueba1;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(MyField; MyField)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
                field(idUnico; idUnico)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

}
