page 50101 "TCN_Prueba01"
{

    Caption = 'TCN_Prueba01';
    PageType = Card;
    SourceTable = TCN_Prueba1;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(MyField; MyField)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
            }
            group(Otros)
            {
                field(idUnico; idUnico)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
