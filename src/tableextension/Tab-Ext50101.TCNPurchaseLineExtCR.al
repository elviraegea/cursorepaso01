tableextension 50101 "TCN_PurchaseLineExtC_R" extends "Purchase Line"
{
    fields
    {
        field(50100; idUnico; Guid)
        {
            DataClassification = ToBeClassified;
            Editable = false;
        }
    }
}
