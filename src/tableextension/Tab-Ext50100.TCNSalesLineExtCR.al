tableextension 50100 "TCN_SalesLineExtC_R" extends "Sales Line"
{
    fields
    {
        field(50100; TCN_IdUnico; Guid)
        {
            DataClassification = ToBeClassified;
        }
    }
    trigger OnInsert()
    begin
        AsignarIdUnicoF();
    end;

    procedure AsignarIdUnicoF()
    begin
        Validate(TCN_IdUnico, CreateGuid());
    end;
}