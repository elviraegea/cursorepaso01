table 50100 "TCN_ComentarioC_R"
{
    DataClassification = ToBeClassified;
    Caption = 'Comentarios';

    fields
    {
        field(1; IdUnicoOrigen; Guid)
        {
            Caption = 'Id único origen';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Nº Línea';
            DataClassification = ToBeClassified;
        }
        field(3; Comentario; Text[250])
        {
            Caption = 'Comentario';
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; IdUnicoOrigen, NumLinea)
        {
            Clustered = true;
        }
    }


}