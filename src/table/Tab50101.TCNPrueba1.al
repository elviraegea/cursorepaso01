table 50101 "TCN_Prueba1"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; MyField; Integer)
        {
            DataClassification = ToBeClassified;

        }
        field(2; Nombre; Text[50])
        {
            DataClassification = ToBeClassified;

        }
        field(3; idUnico; Guid)
        {
            DataClassification = ToBeClassified;
            Editable = false;
        }
    }

    keys
    {
        key(PK; MyField)
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin
        Validate(idUnico, CreateGuid());
    end;
}